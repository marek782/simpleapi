<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 02:27
 */

namespace SimpleApi\Response;


class JsonResponseStrategy implements  ResponseStrategyInterface{

    /**
     * @param $data
     *
     * @return ResponseInterface;
     */
    function createResponse($data)
    {
        new JsonResponse($data);
    }
}