<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 21:56
 */

namespace SimpleApi\Response;


interface ResponseInterface {

    /**
     * @return mixed
     */
    function getBody();

    /**
     * @return mixed
     */
    function getContentType();

    /**
     * @return mixed
     */
    function getHeaders();

    /**
     * @return mixed
     */
    function getStatusCode();

    function addHeaders(array $headers);

    function setStatusCode($code);

    function setBody($data);

    function send();
}