<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 02:01
 */

namespace SimpleApi\Response;

/**
 * response factory
 *
 * Interface ResponseStrategyInterface
 * @package SimpleApi\Response
 */
interface ResponseStrategyInterface {

    /**
     * @param $data
     *
     * @return ResponseInterface;
     */
    function createResponse($data);
}