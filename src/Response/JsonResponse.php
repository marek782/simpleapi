<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 21:59
 */

namespace SimpleApi\Response;


class JsonResponse extends AbstractResponse
{

    const CONTENT_TYPE_JSON = 'text/json';

    private $data = [];


    function __construct(array $data, $headers = []) {

        $this->data = $data;
        $this->addHeaders($headers);
    }

    /**
     * @return mixed
     */
    function getBody()
    {
        $json = json_encode($this->data);

        return ($json) ? $json : "{}";
    }

    /**
     * @return mixed
     */
    function getContentType()
    {
        return self::CONTENT_TYPE_JSON;
    }

    /**
     * @param $data
     */
    function setBody($data)
    {
        $this->data = $data;
    }
}