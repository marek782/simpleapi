<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 22:47
 */

namespace SimpleApi\Response;


abstract class AbstractResponse implements ResponseInterface {

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var array
     */
    protected $headers = [];


    function initDefaultHeaders()
    {
        $this->headers['Content-Length'] = strlen($this->getBody());
        $this->headers['Content-Type'] = $this->getContentType();
    }

    /**
     *
     */
    function getStatusCode()
    {
        return $this->statusCode;
    }

    function addHeaders(array $headers)
    {
        if (!empty($headers))
            $this->headers = array_merge($this->headers, $headers);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    function setStatusCode($code)
    {
        $this->statusCode = $code;
    }

    function send()
    {
        http_send_status($this->statusCode);

        $this->initDefaultHeaders();

        foreach ($this->headers as $k => $v) {
            header($k . ': ' . $v);
        }

        echo $this->getBody();
    }

    function __toString() {

       return $this->send();
    }
}