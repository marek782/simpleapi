<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 22:14
 */

namespace File;

/**
 * Class CsvFileReader
 * @package File
 *
 * todo make interface based
 */
class CsvFileReader
{
    const READ_COLUMN_NAMES_MODE = 'with_columns';
    const READ_DATA_ONLY_MODE = 'data_only';

    /**
     * columns names received from csv file
     *
     * @var array
     */
    protected $imported_columns = array();

    protected $delimiter = ';';

    /**
     * reads data from csv file
     *
     * @param $file_path must be existing file path
     * @param string $read_mode
     * @param int $offset
     * @param int $limit
     * @return array|null array of csv rows except row with column names
     */
    public function getData($file_path, $read_mode = self::READ_COLUMN_NAMES_MODE, $offset = 0, $limit = 100)
    {
        $file_content = $this->readFile($file_path, $offset, $limit);
        $file_content = str_replace(array("\r\n", "\t"), array("\n", " "), $file_content);

        $csv_data = utf8_encode($file_content);

        $data = $this->csvToArray($csv_data);

        if ($read_mode == self::READ_COLUMN_NAMES_MODE && isset($data[0])) {

            $this->imported_columns = $data[0];
            unset($data[0]);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getImportedColumns()
    {
        return $this->imported_columns;
    }

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    protected function csvToArray($csv_data)
    {
        $csv_rows = explode("\n", $csv_data);
        $csv_array = array();

        foreach ($csv_rows as $k => $row) {

            $csv_array_row = str_getcsv($row, $this->delimiter);

            if ($this->isEmptyRow($csv_array_row)) {

                continue;
            }

            $csv_array[$k] = $csv_array_row;
        }

        return $csv_array;
    }

    protected function isEmptyRow($csv_row)
    {

        //find non empty columns
        $filtered = array_filter($csv_row, function($value, $k) {

            return ((!empty($value) && !preg_match('/^(\s)*$/', $value)));

        }, ARRAY_FILTER_USE_BOTH);

        //if $filtered empty = whole row is empty(all columns)
        return (empty($filtered));
    }

    protected function readFile($path, $offset, $lines)
    {
        $file_content = '';
        $i = 0;

        $splFile = new \SplFileObject($path);

        $splFile->seek($offset);

        while ($splFile->valid() && $i < $lines) {

            $file_content .= $splFile->current();
            $splFile->next();
            $i++;
        }

        return $file_content;
    }
}