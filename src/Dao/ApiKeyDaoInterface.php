<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 22:39
 */

namespace Dao;


interface ApiKeyDaoInterface {

    function getAvailableApiKeys();
}