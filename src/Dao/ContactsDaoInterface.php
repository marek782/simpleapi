<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 21:49
 */

namespace Dao;

/**
 * Class ContactsDaoInterface
 */
interface ContactsDaoInterface {


    function getContacts($page = null, $limit = null);
}