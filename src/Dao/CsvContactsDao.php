<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 23:12
 */

namespace Dao;


use File\CsvFileReader;

class CsvContactsDao implements ContactsDaoInterface {

    /**
     * max number of lines
     */
    const MAX_FILE_SIZE = 100000;

    /**
     * @var CsvFileReader
     */
    private $csvReader;

    private $filePath;

    private $readMode;

    function __construct(CsvFileReader $csvFileReader, $file_path, $read_mode = CsvFileReader::READ_DATA_ONLY_MODE)
    {
        $this->csvReader = $csvFileReader;
        $this->filePath = $file_path;
        $this->readMode = $read_mode;
    }

    function getContacts($page = null, $max_per_page = null)
    {
        $limit = ($max_per_page > 0) ? $max_per_page : self::MAX_FILE_SIZE;
        $offset = ($page > 0) ? ($page - 1) * $max_per_page : 0;

        return $this->csvReader->getData($this->filePath, $this->readMode, $offset, $limit);
    }

    /**
     * @return CsvFileReader
     */
    public function getCsvReader()
    {
        return $this->csvReader;
    }

    /**
     * @param CsvFileReader $csvReader
     */
    public function setCsvReader($csvReader)
    {
        $this->csvReader = $csvReader;
    }

    /**
     * @return string
     */
    public function getReadMode()
    {
        return $this->readMode;
    }

    /**
     * @param string $readMode
     */
    public function setReadMode($readMode)
    {
        $this->readMode = $readMode;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }


}