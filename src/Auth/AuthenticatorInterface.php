<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 22:37
 */

namespace Auth;


interface AuthenticatorInterface {

    function authenticate($credentials);

    static function isAuthenticated($identifier);
}