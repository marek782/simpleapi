<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 22:38
 */

namespace Auth;


use Dao\ApiKeyDaoInterface;

class ApiKeyAuthenticator implements AuthenticatorInterface {

    private static $authenticatedKeys = [];

    /**
     * @var ApiKeyDaoInterface
     */
    private $dao;

    function authenticate($api_key)
    {

        if (in_array($api_key, $this->dao->getAvailableApiKeys()));
        {
            self::$authenticatedKeys[] = $api_key;

            return true;
        }

        return false;
    }

    static function isAuthenticated($api_key)
    {
        return in_array($api_key, self::$authenticatedKeys);
    }
}