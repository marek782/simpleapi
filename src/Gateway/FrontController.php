<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 00:25
 */

namespace SimpleApi\Gateway;


use SimpleApi\Request\RequestFactoryInterface;

class FrontController extends GatewayAbstract {


    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;


    function handle($request = null)
    {
        $request = $this->requestFactory->create();

        $this->nextGateway->handle($request);
    }

    /**
     * @return RequestFactoryInterface
     */
    public function getRequestFactory()
    {
        return $this->requestFactory;
    }

    /**
     * @param RequestFactoryInterface $requestFactory
     */
    public function setRequestFactory($requestFactory)
    {
        $this->requestFactory = $requestFactory;
    }


}