<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 00:28
 */

namespace SimpleApi\Gateway;


abstract class GatewayAbstract {

    /**
     * @var GatewayAbstract
     */
    protected $nextGateway;

    /**
     * @param GatewayAbstract $nextGateway
     */
    public function setNextGateway(GatewayAbstract $nextGateway)
    {
        $this->nextGateway = $nextGateway;
    }


    /**
     * @param $request
     * @return mixed
     */
    abstract function handle($request = null);
}