<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 01:01
 */

namespace SimpleApi\Gateway;



use SimpleApi\Request\Request;
use SimpleApi\Response\ResponseInterface;
use SimpleApi\Response\ResponseStrategyInterface;
use SimpleApi\Router\RouteInterface;
use SimpleApi\Router\RouteNotFoundException;
use SimpleApi\Router\RouterInterface;

class ResponseGateway extends GatewayAbstract
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ResponseStrategyInterface[]
     */
    private $responseStrategies = [];

    /**
     * @var ResponseStrategyInterface
     */
    private $defaultStrategy;

    /**
     * @param $request
     * @return mixed
     */
    function handle($request = null)
    {
        $result = $this->router->dispatch();
        $accept = $this->request->getHeader('Accept');

        if ($result instanceof ResponseInterface)
            $result->send();

        else if (isset($this->responseStrategies[$accept]))
        {
            $this->responseStrategies[$accept]->createResponse($result)->send();
        }

        $this->defaultStrategy->creteResponse($result)->send();
    }

    function registerStrategy($content_type, ResponseStrategyInterface $strategy)
    {
        $this->responseStrategies[$content_type] = $strategy;
    }

    /**
     * @return mixed
     */
    public function getDefaultStrategy()
    {
        return $this->defaultStrategy;
    }

    /**
     * @param mixed $defaultStrategy
     */
    public function setDefaultStrategy($defaultStrategy)
    {
        $this->defaultStrategy = $defaultStrategy;
    }
}