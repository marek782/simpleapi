<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 23.02.17
 * Time: 00:40
 */

namespace SimpleApi\Gateway;


use Auth\AuthenticatorInterface;

class AuthGateway extends GatewayAbstract {


    /**
     * @var AuthenticatorInterface
     */
    private $authenticator;

    private $credentials;

    /**
     * @param $request
     * @return mixed
     */
    function handle($request = null)
    {
        if ($this->authenticator->authenticate($this->credentials) && $this->nextGateway) {

            $this->nextGateway->handle();
        }


    }
}