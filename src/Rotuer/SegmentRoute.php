<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 23:24
 */

namespace SimpleApi\Router;



use SimpleApi\Controller\BaseController;

class SegmentRoute implements RouteInterface
{

    private $url;

    private $controller;

    private $action;

    private $routeParams;

    private $allowedMethods = [];

    private $requestUrl;

    private $parsedUrl;

    function __construct(BaseController $controller, $action, $url, $allowed_http_methods = [])
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->url = $url;
        $this->allowedMethods = $allowed_http_methods;

    }

    function match($request_url, $request_method)
    {
        $this->requestUrl = $request_url;
        $this->parsedUrl = $this->parseUrl();

        $method_allowed = (empty($this->allowedMethods)) ? true : in_array($request_method, $this->allowedMethods);

        return ($this->parsedUrl == $request_url && $method_allowed);
    }

    function getRouteParams()
    {
        if (!$this->parsedUrl) {
            $this->parseUrl();
        }

        $this->routeParams['controller'] = get_class($this->controller);
        $this->routeParams['action'] = $this->action;

        return $this->routeParams;
    }

    /**
     * @return BaseController
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getControllerAction()
    {
        return $this->action;
    }

    private function parseUrl()
    {
        $parsed_url = $this->url;

        $matches = [];

        preg_match_all('/\[\:(.*)\]/U' , $this->url, $matches, PREG_OFFSET_CAPTURE);

        if (isset($matches[1])) {

            foreach ($matches[1] as $match) {

                $param_value = $this->getParamValue($match[1], $match[0]);

                $this->routeParams[$match[0]] = $param_value;

                $parsed_url = str_replace('[:' . $match[0] . ']', $param_value, $parsed_url);
            }
        }

        return $parsed_url;
    }

    private function getParamValue($match_offset, $match_value)
    {
        $request_url = ($this->requestUrl) ? $this->requestUrl : $this->controller->getRequest()->getUri();

        return substr($request_url, $match_offset, strlen($match_value));
    }
}