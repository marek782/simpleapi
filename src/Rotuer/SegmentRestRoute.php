<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 23:24
 */

namespace SimpleApi\Router;



use SimpleApi\Controller\BaseController;

class SegmentRestRoute extends SegmentRoute
{


    function __construct(BaseController $controller, $http_method, $url)
    {
        parent::__construct($controller, strtolower($http_method), $url);
    }
}