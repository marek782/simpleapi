<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 21:27
 */

namespace SimpleApi\Router;


use SimpleApi\Request\Request;

class SimpleRouter implements RouterInterface {

    /**
     * @var RouteInterface[]
     */
    private $routes = [];

    /**
     * @var Request
     */
    private $request;

    function __construct(Request $request, $routes)
    {
        $this->route = $routes;
        $this->request = $request;
    }


    function dispatch()
    {
        foreach ($this->routes as $route) {

            if ($route instanceof RouteInterface && $route->match($this->request->getUri(), $this->request->getMethod())) {

                return $this->route->getController()->{$this->route->getControllerAction()};
            }
        }

        throw new RouteNotFoundException();
    }

}