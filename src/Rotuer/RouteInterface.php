<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 23:14
 */

namespace SimpleApi\Router;

use SimpleApi\Controller\BaseController;

interface RouteInterface
{
    function match($request_url, $http_request_method);

    function getRouteParams();

    /**
     * @return BaseController
     */
    function getController();

    function getControllerAction();

}