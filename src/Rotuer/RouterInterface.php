<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 22.02.17
 * Time: 21:26
 */

namespace SimpleApi\Router;


interface RouterInterface {

    /**
     * @return mixed
     * @throws \Exception
     */
    function dispatch();
}