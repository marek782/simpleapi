<?php



namespace SimpleApi;

use SimpleApi\Gateway\AuthGateway;
use SimpleApi\Gateway\FrontController;
use SimpleApi\Request\RequestFactory;
use SimpleApi\Router\SimpleRouter;

class ServiceLoader {

    private static $services = array();

    static function getConfig()
    {
        $config = include realpath(__DIR__ . '/config/config.php');

        return $config;
    }

    static function initGatewayChain()
    {
        $front_controller = new FrontController();
        $auth_gateway = new AuthGateway();

        $front_controller->setNextGateway($auth_gateway);
    }

    static function getRouter()
    {
        $request = self::getRequestFactory()->create();

        $router = new SimpleRouter($request, self::getConfig()['routes']);

        return $router;
    }

    static function getRequestFactory()
    {
        return new RequestFactory();
    }


}