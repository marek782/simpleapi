<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 20.02.17
 * Time: 23:42
 */

namespace SimpleApi\Controller;


abstract class RestController extends BaseController
{

    public abstract function get();

    public abstract function post();

    public abstract function delete();

    public abstract function put($data);
}