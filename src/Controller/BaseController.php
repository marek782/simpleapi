<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 00:05
 */

namespace SimpleApi\Controller;


use SimpleApi\Request\Request;

/**
 * contains common utils for controllers
 *
 * Class BaseController
 * @package SimpleApi\Controller
 */
class BaseController {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var
     */
    protected $responseRenderer;

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getResponseRenderer()
    {
        return $this->responseRenderer;
    }

    /**
     * @param mixed $responseRenderer
     */
    public function setResponseRenderer($responseRenderer)
    {
        $this->responseRenderer = $responseRenderer;
    }


}