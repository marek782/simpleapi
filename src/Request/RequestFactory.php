<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 00:15
 */

namespace SimpleApi\Request;

/**
 * default request factory
 *
 * Class RequestFactory
 * @package SimpleApi\Request
 */
class RequestFactory implements RequestFactoryInterface
{

    /**
     * @return Request
     */
    public function create()
    {
        $request = new Request();

        $request->setContent(file_get_contents("php://input"));

        return $request;
    }
}