<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 00:08
 */

namespace SimpleApi\Request;


class Request {

    const HTTP_METHOD_POST = 'post';
    const HTTP_METHOD_PUT = 'put';
    const HTTP_METHOD_GET = 'get';
    const HTTP_METHOD_DELETE = 'delete';
    const HTTP_METHOD_TRACE = 'trace';
    const HTTP_METHOD_OPTIONS = 'options';
    const HTTP_METHOD_HEAD = 'head';

    /**
     * @var
     */
    protected $content;

    /**
     * @var
     */
    protected $contentType;

    /**
     * @var
     */
    protected $headers;

    /**
     * @var
     */
    protected $method;

    /**
     * @var
     */
    protected $uri;

    /**
     * @var
     */
    protected $postParams;

    protected $queryParams;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param mixed $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    public function getHeader($name, $default = '')
    {
        return (isset($this->headers[$name])) ? $this->headers[$name] : $default;
    }
}