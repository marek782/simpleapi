<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 21.02.17
 * Time: 00:15
 */

namespace SimpleApi\Request;


interface RequestFactoryInterface {

    /**
     * @return Request
     */
    public function create();
}